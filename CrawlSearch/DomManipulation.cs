﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms.VisualStyles;
using HtmlAgilityPack;

namespace CrawlSearch
{
    static class DomManipulation
    {
        public static List<string> ParseLinks(string content, string root)
        {
            root = "http://" + (new Uri(root).Host);

            Regex regexLink = new Regex("(?<=<a\\s*?href=(?:'|\"))[^'\"]*?(?=(?:'|\"))");

            ISet<string> parsedLinks = new HashSet<string>();
            foreach (var matchr in regexLink.Matches(content))
            {
                string match = matchr.ToString();
                if (IsHomepage(match) || !IsValidLink(match))
                {
                    continue;
                }
                match = MakeValidLink(match, root);
                if (!parsedLinks.Contains(match))
                    parsedLinks.Add(match);
            }

            return parsedLinks.ToList();
        }

        public static Boolean IsHomepage(string link)
        {
            switch (link)
            {
                case "/":
                    return true;
                case "./":
                    return true;
                case "../":
                    return true;
                case "/index.php":
                    return true;
                case "/index.html":
                    return true;
                case "index.php":
                    return true;
                case "index.html":
                    return true;
                case "index.htm":
                    return true;
                case "/index.htm":
                    return true;
                case "index.xhtml":
                    return true;
                case "/index.xhtml":
                    return true;
                case "index.jsp":
                    return true;
                case "/index.jsp":
                    return true;
                case "index.do":
                    return true;
                case "/index.do":
                    return true;
                default:
                    return false;
            }
        }

        public static Boolean IsValidLink(string link)
        {
            if (link.Length == 0)
                return false;
            if(link.StartsWith("#"))
                return false;
            if (link.StartsWith("javascript"))
                return false;
            
            return true;
        }

        public static string MakeValidLink(string link, string root)
        {
            if (!link.StartsWith("http"))
            {
                return root + link;
            }
            return link;
        }

        public static string MakeValidStartUrl(string link)
        {
            if (!link.StartsWith("http"))
            {
                return "http://" + link;
            }
            return link;
        }

        public static IEnumerable<string> ExtractLinksFromNode(List<HtmlNode> nodeElements)
        {
            foreach (HtmlNode elem in nodeElements)
            {
                List<HtmlNode> a = elem.Descendants("a").ToList();
                foreach (HtmlNode anchorNode in a)
                {
                    yield return anchorNode.GetAttributeValue("href", "null");
                }
            }
        }

        public static IEnumerable<string> ExtractNextPageLink(string root, List<HtmlNode> nodeElement)
        {
            yield return root + nodeElement.First().GetAttributeValue("href", "null").Substring(2);
        } 

        public static List<HtmlNode> ExtractDescendantsWithClass(HtmlDocument htmlDoc, string descendantType, string className)
        {
            return htmlDoc.DocumentNode.Descendants(descendantType)
                .Where(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Contains(className)).ToList();
        }

        public static string RemoveDiacriticsAndSpaces(this string text)
        {
            var normalizedString = text.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();

            foreach (var c in normalizedString)
            {
                var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
                if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(c);
                }
            }

            string a = stringBuilder.ToString().Normalize(NormalizationForm.FormC);
            Regex pattern = new Regex("[;,  /\\*?\"<>| ]");

            return pattern.Replace(a, "_");
        }

        public static string HtmlTableToJson(List<HtmlNode> productParamTable, HtmlDocument htmlDoc)
        {
            string productParams = "{";
            if (productParamTable.Any())
            {
                foreach (HtmlNode trNode in productParamTable)
                {
                    List<HtmlNode> tdNodes = trNode.Descendants("td").ToList();
                    string paramName = tdNodes[0].Descendants("p").ToList().First().InnerHtml;
                    string id = tdNodes[0].Descendants("p").ToList().First().GetAttributeValue("id","");
                    string valContainerId = "";
                    if (id != "")
                    {
                        valContainerId = id.Replace("name", "value");
                    }
                    string paramValue = htmlDoc.DocumentNode.Descendants()
                        .Where(d => d.Attributes.Contains("id") && d.Attributes["id"].Value.Contains(valContainerId))
                        .ToList()
                        .First()
                        .InnerText;
                    productParams += "{'" + paramName + "':'" + paramValue + "'}";
                }
            }
            productParams += "}";
            return productParams;
        }
    }
}
