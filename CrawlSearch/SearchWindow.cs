﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nest;

namespace CrawlSearch
{
    public partial class SearchWindow : Form
    {
        public SearchWindow()
        {
            InitializeComponent();
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            string elasticUrl = ((MainWindow)this.Owner).getElasticUrl();
            string searchString = this.textBox1.Text;

            var node = new Uri(elasticUrl);
            var settings = new ConnectionSettings(node);
            var client = new ElasticClient(settings);

            IEnumerable<Product> searchResults = null;

            if (radioButtonById.Checked)
            {
                searchResults = client.Search<Product>(s => s
                    .AllIndices()
                    .From(0)
                    .Size(1)
                    .Query(q => q
                        .Term(p => p.ProductId, searchString)
                    )
                ).Documents;
            }


            if (radioButtonByPrice.Checked)
            {
                double from = Int32.Parse(priceFrom.Text);
                double to = Int32.Parse(priceTo.Text);
                searchResults = client.Search<Product>(s => s
                    .AllIndices()
                    .From(0)
                    .Size(100)
                    .Filter(fl => fl
                        .Range(rl => rl
                            .LowerOrEquals(to)
                            .OnField(pl => pl.BestPrice)
                        ) 
                        &&
                        fl.Range(r => r
                            .GreaterOrEquals(from)
                            .OnField(pbp => pbp.BestPrice)
                        )
                    )
                    .SortAscending(sort => sort.BestPrice)
                ).Documents;
            }

            if (radioButtonByTitle.Checked)
            {
                searchResults = client.Search<Product>(s => s
                    .AllIndices()
                    .From(0)
                    .Size(100)
                    .Query(qd => qd
                        .Fuzzy(fz => fz
                            .OnField(f => f.ItemName)
                            .Value(searchString)
                        )
                    )
                ).Documents;
            }

            if (radioButtonTopCategory.Checked)
            {
                searchString = searchString.Trim().ToLower().RemoveDiacriticsAndSpaces();
                searchResults = client.Search<Product>(s => s
                    .Index(searchString)
                    .From(0)
                    .Size(100)
                    .Filter(fd => fd
                        .Range(r => r
                            .Greater(0.0)
                            .OnField(p => p.PriceValue)
                        )
                    )
                    .SortDescending(sort => sort.PriceValue)
                ).Documents;

                if (!searchResults.Any())
                {
                    searchResults = client.Search<Product>(s => s
                    .Index(searchString)
                    .From(0)
                    .Size(100)
                    .Filter(fd => fd
                        .Range(r => r
                            .Greater(0.0)
                            .OnField(p => p.ReviewCount)
                        )
                    )
                    .SortDescending(sd => sd.ReviewCount)
                    ).Documents;
                }

                if (!searchResults.Any())
                {
                    searchResults = client.Search<Product>(s => s
                    .Index(searchString)
                    .From(0)
                    .Size(100)
                    .SortAscending(sd => sd.BestPrice)
                    ).Documents;
                }
            }

            if (radioButtonWhenToBuy.Checked)
            {
                //only the most relevant result
                searchResults = client.Search<Product>(s => s
                    .AllIndices()
                    .From(0)
                    .Size(100)
                    .Query(q =>
                       q.Term(f => f.ItemName, searchString)
                       || q.Term(f => f.ProductDesc, searchString))
                ).Documents;
            }

            UpdateTable(searchResults);

            //show popup after updating table
            if (radioButtonWhenToBuy.Checked && searchResults.Any())
            {
                Product prod = searchResults.First();
                string productTitle = prod.ItemName;
                int estimate = (int) Math.Round(Math.Abs((prod.BestPrice - Decimal.Parse (this.textBoxWhenToBuyFor.Text)) / (decimal)prod.PriceSlope));
                WhenToBuyPopup popup = new WhenToBuyPopup();
                popup.SetProductEstimate(productTitle, estimate);
                popup.ShowDialog(this);
            }
            
            //Console.WriteLine(t.Hits.ToList().First().ToString());
            //ISearchResponse<Product> rr = client.Search<Product>(s => s.AllIndices().Query(q => q.Term(prod => prod.ShopCount, 27)));
            //Console.WriteLine(rr.Hits.ToList().First().ToString());
        }
    }

    public class Product
    {
        public string   ProductId; 
        public decimal  ItemPrice; 
        public decimal  BestPrice; 
        public double   PriceValue; 
        public string   ShopLink; 
        public string   ProductDesc; 
        public string   ProductParams; 
        public string   ItemName; 
        public int      ShopCount; 
        public string   PriceValuesJson; 
        public double   PriceSlope; 
        public int      ReviewCount; 
        public double      RatingValue; 
        public string   Thumbnail;
        public string   Category;

        public Product(
            string productId, 
            decimal itemPrice, 
            decimal bestPrice, 
            double priceValue, 
            string shopLink, 
            string productDesc, 
            string productParams, 
            string itemName, 
            int shopCount, 
            string priceValuesJson, 
            double priceSlope, 
            int reviewCount, 
            double ratingValue, 
            string thumbnail,
            string category)
        {
            ProductId = productId; 
            ItemPrice = itemPrice; 
            BestPrice = bestPrice; 
            PriceValue = priceValue; 
            ShopLink = shopLink; 
            ProductDesc = productDesc; 
            ProductParams = productParams; 
            ItemName = itemName; 
            ShopCount = shopCount; 
            PriceValuesJson = priceValuesJson; 
            PriceSlope = priceSlope; 
            ReviewCount = reviewCount; 
            RatingValue = ratingValue;
            Thumbnail = thumbnail;
            Category = category;
        }
    }
}
