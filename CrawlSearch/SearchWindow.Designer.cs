﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace CrawlSearch
{
    partial class SearchWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        public void RemoveText(object sender, EventArgs e)
        {
                textBox1.Text = "";
        }

        public void AddText(object sender, EventArgs e)
        {
                if(textBox1.Text == "")
                textBox1.Text = "Search...";
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.dataTable = new System.Data.DataTable();
            this.titleDc = new System.Data.DataColumn();
            this.bestPriceDc = new System.Data.DataColumn();
            this.linkDc = new System.Data.DataColumn();
            this.thumbnailDc = new System.Data.DataColumn();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.titleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bestPriceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.linkDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.thumbnailDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.radioButtonById = new System.Windows.Forms.RadioButton();
            this.radioButtonByPrice = new System.Windows.Forms.RadioButton();
            this.priceFrom = new System.Windows.Forms.TextBox();
            this.priceTo = new System.Windows.Forms.TextBox();
            this.radioButtonWhenToBuy = new System.Windows.Forms.RadioButton();
            this.textBoxWhenToBuyFor = new System.Windows.Forms.TextBox();
            this.radioButtonTopCategory = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.radioButtonByTitle = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(13, 13);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(348, 20);
            this.textBox1.TabIndex = 0;
            this.textBox1.Text = "Search...";
            this.textBox1.GotFocus += new System.EventHandler(this.RemoveText);
            this.textBox1.LostFocus += new System.EventHandler(this.AddText);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(368, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(126, 22);
            this.button1.TabIndex = 1;
            this.button1.Text = "Search";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.searchButton_Click);
            // 
            // dataTable
            // 
            this.dataTable.Columns.AddRange(new System.Data.DataColumn[] {
            this.titleDc,
            this.bestPriceDc,
            this.linkDc,
            this.thumbnailDc});
            this.dataTable.TableName = "Products";
            // 
            // titleDc
            // 
            this.titleDc.ColumnName = "Title";
            // 
            // bestPriceDc
            // 
            this.bestPriceDc.ColumnName = "Best price";
            this.bestPriceDc.DataType = typeof(double);
            // 
            // linkDc
            // 
            this.linkDc.ColumnName = "Link";
            // 
            // thumbnailDc
            // 
            this.thumbnailDc.ColumnName = "Thumbnail";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.titleDataGridViewTextBoxColumn,
            this.bestPriceDataGridViewTextBoxColumn,
            this.linkDataGridViewTextBoxColumn,
            this.thumbnailDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.dataTable;
            this.dataGridView1.Location = new System.Drawing.Point(13, 93);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(481, 256);
            this.dataGridView1.TabIndex = 2;
            // 
            // titleDataGridViewTextBoxColumn
            // 
            this.titleDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.titleDataGridViewTextBoxColumn.DataPropertyName = "Title";
            this.titleDataGridViewTextBoxColumn.HeaderText = "Title";
            this.titleDataGridViewTextBoxColumn.Name = "titleDataGridViewTextBoxColumn";
            this.titleDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // bestPriceDataGridViewTextBoxColumn
            // 
            this.bestPriceDataGridViewTextBoxColumn.DataPropertyName = "Best price";
            this.bestPriceDataGridViewTextBoxColumn.HeaderText = "Best price";
            this.bestPriceDataGridViewTextBoxColumn.Name = "bestPriceDataGridViewTextBoxColumn";
            this.bestPriceDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // linkDataGridViewTextBoxColumn
            // 
            this.linkDataGridViewTextBoxColumn.DataPropertyName = "Link";
            this.linkDataGridViewTextBoxColumn.HeaderText = "Link";
            this.linkDataGridViewTextBoxColumn.Name = "linkDataGridViewTextBoxColumn";
            this.linkDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // thumbnailDataGridViewTextBoxColumn
            // 
            this.thumbnailDataGridViewTextBoxColumn.DataPropertyName = "Thumbnail";
            this.thumbnailDataGridViewTextBoxColumn.HeaderText = "Thumbnail";
            this.thumbnailDataGridViewTextBoxColumn.Name = "thumbnailDataGridViewTextBoxColumn";
            this.thumbnailDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // radioButtonById
            // 
            this.radioButtonById.AutoSize = true;
            this.radioButtonById.Checked = true;
            this.radioButtonById.Location = new System.Drawing.Point(13, 43);
            this.radioButtonById.Name = "radioButtonById";
            this.radioButtonById.Size = new System.Drawing.Size(51, 17);
            this.radioButtonById.TabIndex = 3;
            this.radioButtonById.TabStop = true;
            this.radioButtonById.Text = "By ID";
            this.radioButtonById.UseVisualStyleBackColor = true;
            // 
            // radioButtonByPrice
            // 
            this.radioButtonByPrice.AutoSize = true;
            this.radioButtonByPrice.Location = new System.Drawing.Point(13, 66);
            this.radioButtonByPrice.Name = "radioButtonByPrice";
            this.radioButtonByPrice.Size = new System.Drawing.Size(63, 17);
            this.radioButtonByPrice.TabIndex = 4;
            this.radioButtonByPrice.Text = "By price";
            this.radioButtonByPrice.UseVisualStyleBackColor = true;
            // 
            // priceFrom
            // 
            this.priceFrom.Location = new System.Drawing.Point(76, 63);
            this.priceFrom.Name = "priceFrom";
            this.priceFrom.Size = new System.Drawing.Size(28, 20);
            this.priceFrom.TabIndex = 5;
            this.priceFrom.Text = "0";
            // 
            // priceTo
            // 
            this.priceTo.Location = new System.Drawing.Point(116, 63);
            this.priceTo.Name = "priceTo";
            this.priceTo.Size = new System.Drawing.Size(29, 20);
            this.priceTo.TabIndex = 6;
            this.priceTo.Text = "100";
            // 
            // radioButtonWhenToBuy
            // 
            this.radioButtonWhenToBuy.AutoSize = true;
            this.radioButtonWhenToBuy.Location = new System.Drawing.Point(168, 43);
            this.radioButtonWhenToBuy.Name = "radioButtonWhenToBuy";
            this.radioButtonWhenToBuy.Size = new System.Drawing.Size(116, 17);
            this.radioButtonWhenToBuy.TabIndex = 7;
            this.radioButtonWhenToBuy.Text = "When to buy ... for:";
            this.radioButtonWhenToBuy.UseVisualStyleBackColor = true;
            // 
            // textBoxWhenToBuyFor
            // 
            this.textBoxWhenToBuyFor.Location = new System.Drawing.Point(284, 40);
            this.textBoxWhenToBuyFor.Name = "textBoxWhenToBuyFor";
            this.textBoxWhenToBuyFor.Size = new System.Drawing.Size(34, 20);
            this.textBoxWhenToBuyFor.TabIndex = 8;
            this.textBoxWhenToBuyFor.Text = "0";
            // 
            // radioButtonTopCategory
            // 
            this.radioButtonTopCategory.AutoSize = true;
            this.radioButtonTopCategory.Location = new System.Drawing.Point(168, 66);
            this.radioButtonTopCategory.Name = "radioButtonTopCategory";
            this.radioButtonTopCategory.Size = new System.Drawing.Size(126, 17);
            this.radioButtonTopCategory.TabIndex = 9;
            this.radioButtonTopCategory.TabStop = true;
            this.radioButtonTopCategory.Text = "Top rated in category";
            this.radioButtonTopCategory.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(104, 66);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(10, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "-";
            // 
            // radioButtonByTitle
            // 
            this.radioButtonByTitle.AutoSize = true;
            this.radioButtonByTitle.Location = new System.Drawing.Point(335, 43);
            this.radioButtonByTitle.Name = "radioButtonByTitle";
            this.radioButtonByTitle.Size = new System.Drawing.Size(56, 17);
            this.radioButtonByTitle.TabIndex = 11;
            this.radioButtonByTitle.TabStop = true;
            this.radioButtonByTitle.Text = "By title";
            this.radioButtonByTitle.UseVisualStyleBackColor = true;
            // 
            // SearchWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(501, 361);
            this.Controls.Add(this.radioButtonByTitle);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.radioButtonTopCategory);
            this.Controls.Add(this.textBoxWhenToBuyFor);
            this.Controls.Add(this.radioButtonWhenToBuy);
            this.Controls.Add(this.priceTo);
            this.Controls.Add(this.priceFrom);
            this.Controls.Add(this.radioButtonByPrice);
            this.Controls.Add(this.radioButtonById);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox1);
            this.Name = "SearchWindow";
            this.Text = "SearchWindow";
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button1;
        private DataTable dataTable;
        private DataColumn titleDc;
        private DataColumn bestPriceDc;
        private DataColumn linkDc;
        private DataColumn thumbnailDc;
        private DataGridView dataGridView1;
        private DataGridViewTextBoxColumn titleDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn bestPriceDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn linkDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn thumbnailDataGridViewTextBoxColumn;

        public void AddTableRow(TableRow row)
        {
            this.dataTable.Rows.Add(row.Title, row.BestPrice, row.ShopLink, row.ThumbnailLink);
        }

        public void AddTableRow(Product row)
        {
            this.dataTable.Rows.Add(row.ItemName, row.BestPrice, row.ShopLink, row.Thumbnail);
        }

        public void UpdateTable(IEnumerable<Product> list)
        {
            if (list == null || !list.Any())
            {
                Console.Write("ERROR: No results returned for the query!");
                return;
            }
            this.dataTable.Rows.Clear();
            foreach (Product product in list)
            {
                this.AddTableRow(product);
            }

        }

        private RadioButton radioButtonById;
        private RadioButton radioButtonByPrice;
        private TextBox priceFrom;
        private TextBox priceTo;
        private RadioButton radioButtonWhenToBuy;
        private TextBox textBoxWhenToBuyFor;
        private RadioButton radioButtonTopCategory;
        private Label label1;
        private RadioButton radioButtonByTitle;
    }
}