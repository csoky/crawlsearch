﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CrawlSearch
{
    class Gui
    {
        private MainWindow _window;
        private ResourceDistributionSingleton _r;

        public Gui(MainWindow f)
        {
            _window = f;
            _r = ResourceDistributionSingleton.Instance;
        }

        public void UpdateUserInterface()
        {
            while (true)
            {
                _window.rawFileSize.Invoke(new Action(() => _window.rawFileSize.Text = FileManipulation.GetRawFileSize()));
                _window.dataSize.Invoke(new Action(() => _window.dataSize.Text = FileManipulation.GetDataFileSize()));
                _window.stackSize.Invoke(new Action(() => _window.stackSize.Text = _r.UrlStack.Count.ToString()));
                _window.totalCrawledSites.Invoke(new Action(() => _window.totalCrawledSites.Text = _r.VisitedStack.Count.ToString()));
                _window.rawCrawlSpeed.Invoke(new Action(() => _window.rawCrawlSpeed.Text = (FileManipulation.GetRawFileSizeInKB() / _r.GetElapsedTimeFromStart()).ToString()));
                Thread.Sleep(1000);
            }
        }

        public void ResetInterface()
        {
                _window.rawFileSize.Invoke(new Action(() => _window.rawFileSize.Text = "0"));
                _window.dataSize.Invoke(new Action(() => _window.dataSize.Text = "0"));
                _window.stackSize.Invoke(new Action(() => _window.stackSize.Text = "0"));
                _window.totalCrawledSites.Invoke(new Action(() => _window.totalCrawledSites.Text = "0"));
                _window.rawCrawlSpeed.Invoke(new Action(() => _window.rawCrawlSpeed.Text = "0"));
        }

    }

    public class TableRow
    {
        public TableRow(string title, double price, string link, string thumb)
        {
            Title = title;
            BestPrice = price;
            ShopLink = link;
            ThumbnailLink = thumb;
        }

        public string Title { get; set; }
        public double BestPrice { get; set; }
        public string ShopLink { get; set; }
        public string ThumbnailLink { get; set; }
    }

}
