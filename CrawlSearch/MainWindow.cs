﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Net;
using System.IO;
using System.Threading;
using System.Xml.Serialization.Configuration;

namespace CrawlSearch
{
    public partial class MainWindow : Form
    {
        private List<Thread> _threads = new List<Thread>();
        private ResourceDistributionSingleton _r;
 
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            _r = ResourceDistributionSingleton.Instance;
            _r.UrlTable = new LinkedList<string>();
            _r.VisitedTable = new LinkedList<string>();
            _r.VisitedStack = new HashSet<string>();
            _r.UrlStack = new HashSet<string>();
        }

        private void start_Click(object sender, EventArgs e)
        {
            _r.SetStartTime();
            string url = DomManipulation.MakeValidStartUrl(this.textBox1.Text);

            if (_r.UrlTable.Count == 0)
            {
                _r.UrlTableAdd(url);
            }
            if (_r.UrlStack.Count == 0)
            {
                _r.UrlStackAdd(url);
            }

            progressBar1.Style = ProgressBarStyle.Marquee;

            for (int i = 0; i < this.numericUpDown1.Value; i++)
            {
                Crawler cr = new Crawler(this);
                Thread tempThread = new Thread(new ThreadStart(cr.CrawlWeb));
                tempThread.Start();
                _threads.Add(tempThread);
                FileManipulation.LogAction("Thread no. " + tempThread.GetHashCode() + " initialized");
                if (this.numericUpDown1.Value > 1)
                {
                    Thread.Sleep(2000);
                }
            }

            Gui gu = new Gui(this);
            Thread updateThread = new Thread(new ThreadStart(gu.UpdateUserInterface));
            updateThread.Start();
            _threads.Add(updateThread);
        }

        private void pause_Click(object sender, EventArgs e)
        {
            progressBar1.Style = ProgressBarStyle.Continuous;
            foreach (Thread thread in _threads)
            {
                thread.Abort();
            }
            //write each link to one line to file
            FileManipulation.WriteStackToFile(_r.UrlTable);
            FileManipulation.WriteVisitedToFile(_r.VisitedTable);
        }

        private void stopAndReset_Click(object sender, EventArgs e)
        {
            progressBar1.Style = ProgressBarStyle.Continuous;
            foreach (Thread thread in _threads)
            {
                thread.Abort();
            }
            FileManipulation.EraseFiles();
            Gui gu = new Gui(this);
            gu.ResetInterface();
        }

        private void openSearch_Click(object sender, EventArgs e)
        {
            var sw = new SearchWindow();
            sw.ShowDialog(this);
            
        }


    }
}
