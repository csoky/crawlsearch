﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using Nest;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;

namespace CrawlSearch
{
    public class Crawler
    {
        private MainWindow _window;
        private ResourceDistributionSingleton r;

        public Crawler(MainWindow form)
        {
            _window = form;
            r = ResourceDistributionSingleton.Instance;
        }

        public void CrawlWeb()
        {
            int c = r.UrlStack.Count;
            while (c <= 10000000 && c >= 0)
            {
                string a = r.UrlTable.First.Value;
                r.UrlTableRemove(a);
                r.UrlStackRemove(a);
                CrawlUrl(a);
                c = r.UrlStack.Count;
            }
        }

        private void CrawlUrl(string url)
        {
            string result = null;
            WebResponse webResponse = null;
            StreamReader streamReader = null;

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "GET";
                webResponse = request.GetResponse();
                streamReader = new StreamReader(webResponse.GetResponseStream(), Encoding.UTF8);
                result = streamReader.ReadToEnd();
            }
            catch (Exception ex)
            {
                // handle error
                FileManipulation.LogError(FileManipulation.Error, ex.Message + " on site " + url);
                r.VisitedStackAdd(url);
                r.VisitedTableAdd(url);
                r.UrlTableRemove(url);
                r.UrlStackRemove(url);
                if (streamReader != null)
                    streamReader.Close();
                if (webResponse != null)
                    webResponse.Close();
                return;
            }
            FileManipulation.AppendRawResultToFile(result);
            HtmlAgilityPack.HtmlDocument htmlDoc = new HtmlAgilityPack.HtmlDocument();
            htmlDoc.LoadHtml(result);

            List<string> newLinks = new List<string>();
            var mainInfoElement = DomManipulation.ExtractDescendantsWithClass(htmlDoc, "div", "main-info");
            if (mainInfoElement.Count > 0)
            {
                //pravdepodobne sme na stranke elementu, save-ujeme data
                FileManipulation.LogAction("element found on site " + url);
                
                decimal itemPrice = Decimal.Parse(htmlDoc.DocumentNode.Descendants("span")
                    .Where(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Contains("js-top-price")).ToList().First().GetAttributeValue("content", "-1.00"));

                List<HtmlNode> bestPriceStringl = htmlDoc.DocumentNode.Descendants("div")
                    .Where(
                        d =>
                            d.Attributes.Contains("class") && d.Attributes["class"].Value.Contains("shoppr") &&
                            d.Attributes["class"].Value.Contains("first")).ToList();
                string bestPriceString = "";
                if (bestPriceStringl.Any())
                {
                    bestPriceString = bestPriceStringl.First()
                    .Descendants("a")
                    .Where(f => f.Attributes.Contains("class") && f.Attributes["class"].Value.Contains("pricen"))
                    .ToList().First().InnerText;
                }
                else
                {
                    bestPriceString = htmlDoc.DocumentNode.Descendants("div")
                        .Where(
                            d =>
                                d.Attributes.Contains("class") && d.Attributes["class"].Value.Contains("shoppr")).ToList().First()
                        .Descendants("a")
                        .Where(f => f.Attributes.Contains("class") && f.Attributes["class"].Value.Contains("pricen"))
                        .ToList().First().InnerText;
                }
                    
                    //.InnerText.Replace('€', ' ').Trim();
                bestPriceString = Regex.Replace(bestPriceString, "[^\\d,]", "").Replace(',','.');
                decimal bestPrice = Decimal.Parse(bestPriceString);

                List<HtmlNode> shopLinkl = htmlDoc.DocumentNode.Descendants("div")
                    .Where(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Contains("shoppr") && d.Attributes["class"].Value.Contains("first")).ToList();

                string shopLink = "";
                if (shopLinkl.Any())
                {
                    shopLink = shopLinkl.First()
                        .Descendants("div")
                        .Where(rr => rr.Attributes.Contains("class") && rr.Attributes["class"].Value.Contains("buy"))
                        .ToList()
                        .First()
                        .Descendants("a").ToList().First()
                        .GetAttributeValue("href", "http://heureka.sk");
                }
                else
                {
                    shopLink = htmlDoc.DocumentNode.Descendants("div")
                    .Where(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Contains("shoppr")).ToList().First()
                        .Descendants("div")
                        .Where(rr => rr.Attributes.Contains("class") && rr.Attributes["class"].Value.Contains("buy"))
                        .ToList()
                        .First()
                        .Descendants("a").ToList().First()
                        .GetAttributeValue("href", "http://heureka.sk");
                }

                int shopCount = htmlDoc.DocumentNode.Descendants("div")
                    .Where(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Contains("shoppr")).ToList().Count;

                List<HtmlNode> productDescn = htmlDoc.DocumentNode.Descendants("div")
                    .Where(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Contains("product-desc")).ToList();
                string productDesc = "";
                if (productDescn.Any())
                {
                    productDesc = productDescn.First()
                    .Descendants("p").ToList().First().InnerHtml;
                }
                

                List<HtmlNode> productParamTable = htmlDoc.DocumentNode.Descendants("table")
                    .Where(d => d.Attributes.Contains("id") && d.Attributes["id"].Value.Contains("product-parameters")).ToList();
                string productParams = "{}";
                if (productParamTable.Any())
                {
                    productParams = DomManipulation.HtmlTableToJson(productParamTable.First().Descendants("tr").ToList(), htmlDoc);
                }
                string productId = htmlDoc.DocumentNode.Descendants("div")
                    .Where(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Contains("graph-item")).ToList().First()
                    .Descendants("a").ToList().First().GetAttributeValue("href", "http://heureka.sk/preview.php?id=0").Split('?').Last().Substring(3);

                //make ajax request to retrieve price chart http://heureka.sk/direct/ajax/product/price-chart/?pid=56218319
                string priceValuesJsonString = "";
                try
                {
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://heureka.sk/direct/ajax/product/price-chart/?pid="+productId);
                    request.Method = "GET";
                    webResponse = request.GetResponse();
                    streamReader = new StreamReader(webResponse.GetResponseStream(), Encoding.UTF8);
                    priceValuesJsonString = streamReader.ReadToEnd();
                }
                catch (Exception ex)
                {
                    FileManipulation.LogError(FileManipulation.Error, ex.Message + " AJAX price requeston site " + "http://heureka.sk/direct/ajax/product/price-chart/?pid=" + productId);
                    streamReader.Close();
                    webResponse.Close();
                }

                string priceValuesJson = JsonConvert.SerializeObject(priceValuesJsonString, Formatting.Indented);

                string itemName = htmlDoc.DocumentNode.Descendants("div")
                    .Where(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Contains("text-cover"))
                    .ToList().First().ChildNodes.Where(cn => cn.Descendants("h1").Any()).ToList().First().Descendants("h1").First().InnerHtml;

                List<HtmlNode> reviewCountNode = htmlDoc.DocumentNode.Descendants("span")
                    .Where(
                        d => d.Attributes.Contains("itemprop") && d.Attributes["itemprop"].Value.Contains("reviewCount"))
                    .ToList();
                int reviewCount = 0;
                if(reviewCountNode.Any())
                    reviewCount = Int32.Parse(reviewCountNode.First().InnerHtml);

                List<HtmlNode> ratingValueNode = htmlDoc.DocumentNode.Descendants("span")
                    .Where(
                        d => d.Attributes.Contains("itemprop") && d.Attributes["itemprop"].Value.Contains("ratingValue"))
                    .ToList();
                int ratingValue = 0;
                if(ratingValueNode.Any())
                    ratingValue = Int32.Parse(ratingValueNode.First().InnerHtml.Replace("%",""));

                string thumbnail = htmlDoc.DocumentNode.Descendants("img")
                    .Where(d => d.Attributes.Contains("id") && d.Attributes["id"].Value.Contains("picture-main"))
                    .ToList()
                    .First()
                    .GetAttributeValue("src","http://heureka.sk");

                string categoryNodeRaw = htmlDoc.DocumentNode.Descendants("link")
                    .Where(
                        d => d.Attributes.Contains("itemprop") && d.Attributes["itemprop"].Value.Contains("category"))
                    .ToList().First().GetAttributeValue("content","Elektronika");

                string[] categories = categoryNodeRaw.Split('/');
                //string index = "";
                string type = "";
                if (categories.Length < 2)
                {
                    //index = "elektronika";
                    type = categories.Last().Trim().ToLower().RemoveDiacriticsAndSpaces();
                }
                else
                {
                    //index = categories[categories.Length - 2].Trim().ToLower().RemoveDiacriticsAndSpaces();
                    type = categories.Last().Trim().ToLower().RemoveDiacriticsAndSpaces();
                }

                double priceValue = 0;
                if (ratingValue != 0 && reviewCount != 0)
                {
                    //almost-weighted
                    priceValue = (ratingValue/100)*(1 + ((reviewCount/100)));
                }
                double slope = 0;
                if (priceValuesJsonString != "")
                {
                    JToken token = JObject.Parse(priceValuesJsonString);
                    var mins = token.SelectToken("min").ToList();
                    var dates = token.SelectToken("dates").ToString().Split(',');
                    double firstMin = Double.Parse(mins.First().ToString());
                    double lastMin = Double.Parse(mins.Last().ToString());
                    var fDate = Regex.Replace(dates.First(), "[^\\d.]", "").Split('.');
                    DateTime firstDate = new DateTime(Int32.Parse(fDate[2]), Int32.Parse(fDate[1]), Int32.Parse(fDate[0]));

                    var lDate = Regex.Replace(dates.Last(), "[^\\d.]", "").Split('.');
                    DateTime lastDate = new DateTime(Int32.Parse(lDate[2]), Int32.Parse(lDate[1]), Int32.Parse(lDate[0]));

                    double dateDiff = (lastDate - firstDate).TotalDays;

                    slope = (firstMin - lastMin) / (1.0 - dateDiff);
                }
                var product = new Product(
                    productId,
                    itemPrice,
                    bestPrice,
                    priceValue,
                    shopLink,
                    productDesc,
                    productParams,
                    itemName,
                    shopCount,
                    priceValuesJson,
                    slope,
                    reviewCount,
                    ratingValue,
                    thumbnail,
                    type
                );
                FileManipulation.AppendData(product);

                var node = new Uri(_window.getElasticUrl());
                var settings = new ConnectionSettings(node);
                var client = new ElasticClient(settings);

                client.Index<Product>(product, i => i.Index(type));
            }
            else
            {
                var subsecElements = DomManipulation.ExtractDescendantsWithClass(htmlDoc, "div", "subsec");
                if (subsecElements.Count > 0)
                {
                    //existuje subsekcia, o nic ine sa nestarame
                    newLinks.AddRange(DomManipulation.ExtractLinksFromNode(subsecElements));
                }
                else
                {
                    var catlistElements = DomManipulation.ExtractDescendantsWithClass(htmlDoc, "ul", "catlist");
                    if (catlistElements.Count > 0)
                    {
                        //neexistuje subsekcia, takze zoberie hlavnu sekciu
                        newLinks.AddRange(DomManipulation.ExtractLinksFromNode(catlistElements));
                    }
                    else
                    {
                        //neexistuje subsekcia a ani hlavna, zoberieme zoznam tovarov
                        var productContainerElements = DomManipulation.ExtractDescendantsWithClass(htmlDoc, "div", "product-container");
                        if (productContainerElements.Count > 0)
                        {
                            newLinks.AddRange(DomManipulation.ExtractLinksFromNode(productContainerElements));
                            //taktiez ulozime link na pager (next), ak existuje
                            var nextPageElement = DomManipulation.ExtractDescendantsWithClass(htmlDoc, "a", "next");
                            if(nextPageElement.Count > 0)
                                newLinks.AddRange(DomManipulation.ExtractNextPageLink(url, nextPageElement));
                        }
                        else
                        {
                            //wtf stranku sme naparsovali. pravdepodobne odtialto nepotrebujeme nic. zalogujem a koncim, cau.
                            FileManipulation.LogError(FileManipulation.Error, "nothing usable found on site:" + url);
                        }
                    }
                }
            }

            foreach (string newLink in newLinks)
            {
                if (!r.UrlStack.Contains(newLink) && !r.VisitedStack.Contains(newLink))
                {
                    r.UrlStackAdd(newLink);
                    r.UrlTableAdd(newLink);
                }
            }
            r.VisitedStackAdd(url);
            r.VisitedTableAdd(url);
            FileManipulation.LogAction("Just visited URL: "+url);
        }
    }
}