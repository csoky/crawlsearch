﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CrawlSearch
{
    public partial class WhenToBuyPopup : Form
    {
        public WhenToBuyPopup()
        {
            InitializeComponent();
        }

        public void SetProductEstimate(string productTitle, int estimate)
        {
            this.labelDays.Text = estimate.ToString();
            this.labelProductTitle.Text = productTitle;
        }
    }
}
