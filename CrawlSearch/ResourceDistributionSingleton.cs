﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace CrawlSearch
{
    public sealed class ResourceDistributionSingleton
    {
        private static volatile ResourceDistributionSingleton _instance;
        private static readonly object SyncRoot = new Object();
        private static readonly object UrlStackLock = new Object();
        private static readonly object VisitedStackLock = new Object();
        private static readonly object UrlTableLock = new Object();
        private static readonly object VisitedTableLock = new Object();

        private ResourceDistributionSingleton() { }

        private static HashSet<string> _urlStack;
        private static HashSet<string> _visitedStack;
        private static LinkedList<string> _urlTable;
        private static LinkedList<string> _visitedTable;
        public static DateTime StartTime { get; set; }

        public void SetStartTime()
        {
            StartTime = DateTime.Now;
        }

        public int GetElapsedTimeFromStart()
        {
            var val = (int) (DateTime.Now - StartTime).TotalSeconds;
            return (val == 0) ? 1 : val;
        }

        public HashSet<string> UrlStack
        {
            get
            {
                lock (UrlStackLock)
                {
                    return _urlStack;   
                }
            }
            set
            {
                lock (UrlStackLock)
                {
                    _urlStack = value;
                }
            }
        }

        public HashSet<string> VisitedStack
        {
            get
            {
                lock (VisitedStackLock)
                {
                    return _visitedStack;
                }
            }
            set
            {
                lock (VisitedStackLock)
                {
                    _visitedStack = value;
                }
            }
        }

        public LinkedList<string> UrlTable
        {
            get
            {
                lock (UrlTableLock)
                {
                    return _urlTable;
                }
            }
            set
            {
                lock (UrlTableLock)
                {
                    _urlTable = value;
                }
            }
        }

        public LinkedList<string> VisitedTable
        {
            get
            {
                lock (VisitedTableLock)
                {
                    return _visitedTable;
                }
            }
            set
            {
                lock (VisitedTableLock)
                {
                    _visitedTable = value;
                }
            }
        }

        public static ResourceDistributionSingleton Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (SyncRoot)
                    {
                        if (_instance == null)
                            _instance = new ResourceDistributionSingleton();
                    }
                }

                return _instance;
            }
        }

        private static void UrlStackAddS(string a)
        {
            lock (UrlStackLock)
            {
                _urlStack.Add(a);
            }
        }

        private static void UrlTableAddS(string a)
        {
            lock (UrlTableLock)
            {
                _urlTable.AddLast(a);
            }
        }

        private static void VisitedStackAddS(string a)
        {
            lock (VisitedStackLock)
            {
                _visitedStack.Add(a);
            }
        }

        private static void VisitedTableAddS(string a)
        {
            lock (VisitedTableLock)
            {
                _visitedTable.AddLast(a);
            }
        }


        private static void UrlStackRemoveS(string a)
        {
            lock (UrlStackLock)
            {
                _urlStack.Remove(a);
            }
        }

        private static void UrlTableRemoveS(string a)
        {
            lock (UrlTableLock)
            {
                _urlTable.Remove(a);
            }
        }

        private static void VisitedStackRemoveS(string a)
        {
            lock (VisitedStackLock)
            {
                _visitedStack.Remove(a);
            }
        }

        private static void VisitedTableRemoveS(string a)
        {
            lock (VisitedTableLock)
            {
                _visitedTable.Remove(a);
            }
        }




        public void VisitedStackAdd(string a)
        {
            VisitedStackAddS(a);
        }
        public void UrlStackAdd(string a)
        {
            UrlStackAddS(a);
        }
        public void VisitedTableAdd(string a)
        {
            VisitedTableAddS(a);
        }
        public void UrlTableAdd(string a)
        {
            UrlTableAddS(a);
        }
        public void VisitedStackRemove(string a)
        {
            VisitedStackRemoveS(a);
        }
        public void UrlStackRemove(string a)
        {
            UrlStackRemoveS(a);
        }
        public void VisitedTableRemove(string a)
        {
            VisitedTableRemoveS(a);
        }
        public void UrlTableRemove(string a)
        {
            UrlTableRemoveS(a);
        }
    }
}