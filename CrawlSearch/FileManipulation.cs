﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CrawlSearch
{
    class FileManipulation
    {
        private static readonly Object RawFileLock = new Object();
        private static readonly Object StackFileLock = new Object();
        private static readonly Object VisitedFileLock = new Object();
        private static readonly Object ErrLogFileLock = new Object();
        private static readonly Object LogFileLock = new Object();
        private static readonly Object DataFileLock = new Object();
        private static readonly string _rawPath = @"d:\CrawlSearch\raw_pages.txt";
        private static readonly string _stackPath = @"d:\CrawlSearch\url_unvisited_stack.txt";
        private static readonly string _visitedPath = @"d:\CrawlSearch\url_visited_list.txt";
        private static readonly string _errorLogPath = @"d:\CrawlSearch\errors_and_warning.txt";
        private static readonly string _actionLogPath = @"d:\CrawlSearch\actions.txt";
        private static readonly string _dataPath = @"d:\CrawlSearch\data.txt";

        public const string Error = "ERROR";
        public const string Warning = "WARNING";

        public static string GetDataFileName()
        {
            return _dataPath;
        }

        public static int GetRawFileSizeInKB()
        {
            if (!File.Exists(_rawPath))
            {
                return 0;
            }
            double len = new FileInfo(_rawPath).Length;
            return (int) (len/1024);
        }

        public static string GetRawFileSize()
        {
            return GetFileSize(_rawPath);
        }

        public static string GetDataFileSize()
        {
            return GetFileSize(_dataPath);
        }

        private static string GetFileSize(string filename)
        {
            string[] sizes = { "B", "KB", "MB", "GB" };
            if (!File.Exists(filename))
            {
                return "0 KB";
            }
            double len = new FileInfo(filename).Length;
            int order = 0;
            while (len >= 1024 && order + 1 < sizes.Length)
            {
                order++;
                len = len / 1024;
            }

            // Adjust the format string to your preferences. For example "{0:0.#}{1}" would
            // show a single decimal place, and no space.
            return String.Format("{0:0.##} {1}", len, sizes[order]);
        }
        
        public static void LogAction(string message)
        {
            lock (LogFileLock)
            {
                File.AppendAllText(_actionLogPath, DateTime.Now + " - " + message + "\n");
            }
        }

        public static void LogError(string severity, string message)
        {
            lock (ErrLogFileLock)
            {
                File.AppendAllText(_errorLogPath, severity + " - " + DateTime.Now + " " + message + "\n");
            }  
        }
        
        public static void AppendRawResultToFile(string r)
        {
            lock (RawFileLock)
            {
                File.AppendAllText(_rawPath, r);
            }   
        }

        public static void AppendData(object r)
        {
            JsonSerializer serializer = new JsonSerializer();
            
            lock (DataFileLock)
            {
                using (StreamWriter sw = new StreamWriter(_dataPath, true))
                using (JsonWriter writer = new JsonTextWriter(sw))
                {
                    serializer.Serialize(writer, r);
                    writer.WriteWhitespace("\n");
                }
            } 
        }

        public static void EraseFiles()
        {
            //erase all except data files
            lock (RawFileLock)
            {
                File.Delete(_rawPath);
            }  
            lock (StackFileLock)
            {
                File.Delete(_stackPath);
            }
            lock (VisitedFileLock)
            {
                File.Delete(_visitedPath);
            }
            lock (ErrLogFileLock)
            {
                File.Delete(_errorLogPath);
            }
            lock (LogFileLock)
            {
                File.Delete(_actionLogPath);
            }
        }

        //should only be called once after terminating
        public static void WriteStackToFile(LinkedList<string> a)
        {
            StringBuilder sb = new StringBuilder();
            foreach (string s in a)
            {
                sb.Append(s+"\n");
            }
            lock (StackFileLock)
            {
                File.WriteAllText(_stackPath, sb.ToString());
            }
        }

        //should only be called once after terminating
        public static void WriteVisitedToFile(LinkedList<string> r)
        {
            StringBuilder sb = new StringBuilder();
            foreach (string s in r)
            {
                sb.Append(s + "\n");
            }
            lock (VisitedFileLock)
            {
                File.WriteAllText(_visitedPath, sb.ToString());
            }
        }
    }
}
